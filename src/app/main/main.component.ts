import { Component, OnInit, ViewChild } from '@angular/core';
import { IQuestion } from '../question/question';
import { Quiz } from '../question/quiz';
import { IQuestionStatus } from '../question/questionStatus';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { MainDialogComponent } from './main-dialog.component';
import { Mark } from '../question/mark';
import { TestBed } from '@angular/core/testing';
import { QuizGenerator } from '../guizGenerator/quizGenerator';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  startQuiz: boolean = false;

  verify: boolean=false;

  title: string = "Exerciții de scăderi și adunări";

  
  onStart(nbQuestions: number): void{
    let generator = new QuizGenerator();
    this.quiz = generator.CreateQuiz(nbQuestions);
    this.startQuiz = true;
  }
 
  openMainDialog(mark: Mark): void{
    const dialogRef = this.dialog.open(MainDialogComponent, {
      width: '200px',
      data: mark
    })
  }

  onVerifyClicked(): void{
    if(this.verify){
      this.startQuiz = false;
      this.verify = false;
    }
    if(this.startQuiz){
      if(this.quiz.QuizCompleted()){
        this.verify = true;
        this.openMainDialog(this.quiz.GetMark());
      }
      else{ 
        this._snackBar.open("Nu ai terminat toate exercițiile!", "", {
          duration: 5 * 1000,
          verticalPosition: "top",
          panelClass: ['snackCenter']
        })
      }
    }
  }

  onStatusChange(status:IQuestionStatus, index: number): void{
    this.quiz.questionStatus[index] = status;
  }

  @ViewChild('gridView') gridView;

  nbColumns: number = 0;
  setColumns(width: number){
    if(width < 300 ){
      this.nbColumns = 1;
      return;
    }
    if(width < 600 ){
      this.nbColumns = 2;
      return;
    }
    if(width < 900 ){
      this.nbColumns = 3;
      return;
    }
    if(width < 1200 ){
      this.nbColumns = 4;
      return;
    }
    if(width < 1500 ){
      this.nbColumns = 5;
      return;
    }
    if(width < 1800 ){
      this.nbColumns = 6;
      return;
    }
    if(width < 2100 ){
      this.nbColumns = 7;
      return;
    }
    if(width < 2400 ){
      this.nbColumns = 8;
      return;
    }
    this.nbColumns = 9;
  }
  onResize(event) {
    let width = event.target.innerWidth;
    console.log(width);
    this.setColumns(width);
  }


  ngOnInit(): void {
    this.setColumns(window.innerWidth)
  }

  questions: IQuestion[] =[
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },
    {
      message: "23 - 7 =",
      choices: ["18", "16", "19", "14"],
      correctAnswer: 1
    },

  ];
  quiz: Quiz = new Quiz(this.questions);
}
