import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Mark } from '../question/mark';

@Component({
    selector: 'main-dialog',
    templateUrl: 'main-dialog.component.html',
  })
  export class MainDialogComponent {
  
    constructor(
        public dialogRef: MatDialogRef<MainDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public mark: Mark) {}
    
      onNoClick(): void {
        this.dialogRef.close();
      }
  }