import { Quiz } from '../question/quiz';
import { IQuestion } from '../question/question';

export class QuizGenerator {
    nbQuestions: number;

    min: number = 10;
    max: number = 100;
    constructor() {
        
    }

    private shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    private getRandomInt(min, max): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
      }

      private generateUniqueNumbers(nb: number, min, max): number[]{
        var arr = [];
        while(arr.length < nb){
            var r = this.getRandomInt(min, max); 
            if(arr.indexOf(r) === -1) arr.push(r);
        }
        return arr;
      }

    private CreateChoices(correct:number) : string[] {
        let nbChoices = 4;
        let max = correct - 11 < 0 ? correct - 1 : 11
        let deltaCorect = this.generateUniqueNumbers(nbChoices - 1, -11,max);
        let choices = [];
        choices.push(correct.toString());
        for (const delta of deltaCorect) {
            choices.push((correct - delta).toString());
        }
        return this.shuffle(choices);
    }

    private GenerateChoices(numberOfChoices: number, correct: number, min: number, max: number) : string[]{
        //min and max are relative to number
        // min has to be <0
        let allChoices = [];
        for (let index = correct + min; index <= correct + max; index++) {
            if(index >= 0 && index !== correct){
                allChoices.push(index.toString());
            }
        }
        allChoices = this.shuffle(allChoices);
        let choices = allChoices.slice(0,numberOfChoices);
        choices[0] = correct.toString();
        return this.shuffle(choices);
    }
    private CreateQuestion() : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, first);
        let choices = this.GenerateChoices(5, first-second, -11, 11);
        return {
            message: `${first} - ${second} =`,
            correctAnswer: choices.indexOf((first-second).toString()),
            choices: choices
        }
    }

    private AdditionQuestion(numberOfChoices: number) : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, this.max);
        let result = first + second;
        let choices = this.GenerateChoices(numberOfChoices, result, -11, 11);
        return{
            message: `${first} + ${second} =`,
            correctAnswer: choices.indexOf(result.toString()),
            choices: choices
        }
    }

    private SubstractQuestion(numberOfChoices: number) : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, first);
        let result = first - second;
        let choices = this.GenerateChoices(numberOfChoices, result, -11, 11);
        return{
            message: `${first} - ${second} =`,
            correctAnswer: choices.indexOf(result.toString()),
            choices: choices
        }
    }

    private EquationDiffFirstQuestion(numberOfChoices: number) : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, this.max);
        let result = second + first;
        let choices = this.GenerateChoices(numberOfChoices, result, -11, 11);
        return{
            message: `x - ${first} = ${second}`,
            correctAnswer: choices.indexOf(result.toString()),
            choices: choices
        }
    }

    private EquationDiffSecondQuestion(numberOfChoices: number) : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, first);
        let result = first - second;
        let choices = this.GenerateChoices(numberOfChoices, result, -11, 11);
        return{
            message: `${first} - x = ${second}`, 
            correctAnswer: choices.indexOf(result.toString()),
            choices: choices
        }
    }

    private EquationAddMessage(first : number, result : number) : string{
        if( Math.random() < 0.5 ){
            return `${first} + x = ${result}`;
        }
        return `x + ${first} = ${result}`
    }

    private EquationAddQuestion(numberOfChoices: number ) : IQuestion {
        let first = this.getRandomInt(this.min,this.max);
        let second = this.getRandomInt(this.min, first);
        let result = first - second;
        let choices = this.GenerateChoices(numberOfChoices, result, -11, 11);
        return{
            message: this.EquationAddMessage(second, first),
            correctAnswer: choices.indexOf(result.toString()),
            choices: choices
        }
    }

    private RandomQuestion(numberOfChoices: number) : IQuestion {
        let questionType = Math.random();
        if(questionType < 0.25){
            return this.SubstractQuestion(numberOfChoices);
        }
        if(questionType < 0.5){
            return this.AdditionQuestion(numberOfChoices);
        }
        if(questionType < 0.75){
            if(Math.random() <0.5){
                return this.EquationDiffFirstQuestion(numberOfChoices);
            }
            return this.EquationDiffSecondQuestion(numberOfChoices);
        }
        return this.EquationAddQuestion(numberOfChoices);
    }

    public CreateQuiz(nbQuestions: number) : Quiz{
        let questions = [];
        for (let index = 0; index < nbQuestions; index++) {

            questions.push(this.RandomQuestion(4));
        }
        return new Quiz(questions);
    }
}