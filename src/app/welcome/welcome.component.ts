import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  

  @Output() startClicked: EventEmitter<number>= new EventEmitter<number>();
  
  constructor(private _snackBar: MatSnackBar) { }
  
  ngOnInit(): void {
  }

  createSnack(message: string): void{
    this._snackBar.open(message, "", {
      duration: 5 * 1000,
      verticalPosition: "top",
      panelClass: ['snackCenter']
    })
  }

  onStart(nbQuestions: string): void{
    if(nbQuestions === "" || nbQuestions.length === 0){
      this.createSnack("Nu ai introdus un număr")
      return;
    }
    let nb = Math.floor(Number(nbQuestions));
    if(isNaN(nb)){  
      this.createSnack(`${nbQuestions} nu este un număr`);
      return;
    }
    this.startClicked.emit(nb);
  }

}
