import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { IQuestion } from './question';
import { IChoiceButton } from './choiceButton';
import { IQuestionStatus } from './questionStatus';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  constructor() { }
  ngOnChanges(): void {
    if(this.verify) {
        this.status.emit({
          slected: this.isSelected,
          correct: this.isCorrect
        })
      }
  }

  @Input() question: IQuestion = null;

  choiceButtons: IChoiceButton[] = [];
  @Input() verify: boolean = false;
  @Output() status: EventEmitter<IQuestionStatus> = new EventEmitter<IQuestionStatus>();

  isCorrect: boolean = false;
  isSelected: boolean = false;

  resetButtons(): void{
    this.choiceButtons.forEach(button => {
      button.toggled = false;
      button.colorClass = "";
    });
  }

  newChoice(index: number): void{
    this.isSelected = true;
    if(this.verify == false){
      this.resetButtons();
      this.choiceButtons[index].toggled = true;
      if(this.question.correctAnswer == index){
        this.isCorrect = true;
      }
      else{
        this.isCorrect = false;
      }
    }
    this.status.emit({
      slected: this.isSelected,
      correct: this.isCorrect
    })
  }

  changeColor(index: number): String{
    if(this.verify){
      if(index == this.question.correctAnswer){
        return "buttonCorrect";
      }
      if(this.choiceButtons[index].toggled){
        return "buttonIncorect";
      }
      return "";
    }
    if (this.choiceButtons[index].toggled) {
      return "buttonToggled";
    }
    return "";
  }

  ngOnInit(): void {
    this.question.choices.forEach(choice => {
      this.choiceButtons.push({
        toggled: false,
        colorClass: ""
      });
    });
  }

}
