import { IQuestion } from './question';
import { IQuestionStatus } from './questionStatus';
import { Mark } from './mark';

export class Quiz {
    questions: IQuestion[];
    mark: Mark;
    questionStatus: IQuestionStatus[] = [];

    constructor(questions : IQuestion[]) {
        this.questions = questions;
        questions.forEach(quest => {
            this.questionStatus.push({
                slected: false,
                correct: false
            })
        });
    }

    /**
     * QuizCompleted
     */
    public QuizCompleted(): boolean {
        for (const quest of this.questionStatus) {
            if(quest.slected == false)
                return false;
        }
        return true;
    }

    public GetMark(): Mark {
        if(!this.QuizCompleted){
            return null;
        }
        let nbCorect = 0;
        let total = 0;
        for (const quest of this.questionStatus) {
            total++;
            if(quest.correct)
                nbCorect++;
        }
        this.mark = new Mark(nbCorect, total);
        return this.mark;

    }
}