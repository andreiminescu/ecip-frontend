export interface IQuestion {
    message: string;
    correctAnswer: number;
    choices: string[];
}