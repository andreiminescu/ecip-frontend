export interface IChoiceButton {
    toggled: boolean;
    colorClass: string;
}