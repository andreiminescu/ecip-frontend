export class Mark {
    total: number;
    nbCorrect: number;
    mark: number;

    constructor(nbCorrect: number, total: number){
        this.total = total;
        this.nbCorrect = nbCorrect;
        if(nbCorrect == 0 && total != 0){
            this.mark = 0;
        } else{
            this.mark = nbCorrect / total;
        }
    }
}